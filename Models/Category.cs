namespace Zadanie_1._1.Models;

public class Category
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public string? AdditionalPar { get; set; }
}