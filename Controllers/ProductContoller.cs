using Microsoft.AspNetCore.Mvc;
using Zadanie_1._1.Models;
using Zadanie_1._1.Persistence;

namespace Zadanie_1._1.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class ProductContoller : ControllerBase
{
    [HttpGet]
    public IActionResult GetProducts() =>
        Ok(ProductRepository.Products);

    [HttpGet("Id")]
    public IActionResult GetProductById(int Id) =>
        Ok(ProductRepository.Products.FirstOrDefault(product => product.Id == Id));

    [HttpDelete("Id")]
    public IActionResult DeleteById(int id)
    {
        var product = ProductRepository.Products.FirstOrDefault(product => product.Id == id);
        if (product is { })
        {
            ProductRepository.Products.Remove(product);
        }

        return Ok();
    }
    [HttpPost]
    public IActionResult Create(Product product)
    {
        if (ProductRepository.Products.Any(c => c.Name == product.Name))
        {
            throw new InvalidOperationException("Already exist");
        }
        
        product.Id = ProductRepository.GetNewId();
        product.CategoryId = ProductRepository.GetNewId();
        product.Category.Id = product.CategoryId;
        ProductRepository.Products.Add(product);
        var category = new Category{Id = product.CategoryId, Name = product.Category.Name, AdditionalPar = product.Category.AdditionalPar};
        CategoryRepository.Categories.Add(category);
        return Ok();
    }

    [HttpPut]
    public IActionResult Search(string name)
    {
        List<Product> pro = new List<Product>();
        foreach (var product in ProductRepository.Products)
        {
            if (name.ToLower() == product.Category.Name.ToLower() || name.ToLower() == product.Category.AdditionalPar.ToLower())
            {
                pro.Add(product);
            }
            continue;
        }
        return Ok(pro);
    }
}