using Microsoft.AspNetCore.Mvc;
using Zadanie_1._1.Models;
using Zadanie_1._1.Persistence;

namespace Zadanie_1._1.Controllers;

[ApiController]
[Route("api/[controller]/[action]")]
public class CategoryController : ControllerBase
{
    [HttpGet]
    public IActionResult GetCategories() =>
        Ok(CategoryRepository.Categories);

    [HttpGet("{id}")]
    public IActionResult GetCategoryById(int id) =>
        Ok(CategoryRepository.Categories.FirstOrDefault(category => category.Id == id));

    [HttpDelete("{id}")]
    public IActionResult Remove(int id)
    {
        var category = CategoryRepository.Categories.FirstOrDefault(category => category.Id == id);

        if (category is { })
        {
            CategoryRepository.Categories.Remove(category);
        }

        return Ok();
    }

    [HttpPost]
    public IActionResult Create(Category category)
    {
        if (CategoryRepository.Categories.Any(c => c.Name == category.Name))
        {
            throw new InvalidOperationException("Already exist");
        }
        
        category.Id = CategoryRepository.GetNewId();
        CategoryRepository.Categories.Add(category);
        
        return Ok();
    }
}