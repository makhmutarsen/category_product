using Zadanie_1._1.Models;
using Zadanie_1._1.Persistence;

var categories = new List<Category>
{
    new() {Id = 1, Name = "Размер"},
    new() {Id = 2, Name = "Цвет"},
    new() {Id = 3, Name = "Цена"},
    new() {Id = 4, Name = "Новинка"},
    new() {Id = 5, Name = "В тренде"},
};

var products = new List<Product>
{
    new()
    {
        Id = 1, Name = "Рубашка", Category = CategoryRepository.Categories[0], CategoryId = CategoryRepository.Categories[0].Id,
        Description = "Рубашка белого цвета, Размер XL"
    },
    new()
    {
        Id = 2, Name = "Футболка", Category = CategoryRepository.Categories[1], CategoryId = CategoryRepository.Categories[1].Id,
        Description = "Футболка черного цвета, Размер L, Новинка"
    },
    new()
    {
        Id = 3, Name = "Брюки", Category = CategoryRepository.Categories[2], CategoryId = CategoryRepository.Categories[2].Id,
        Description = "Брюки бежового цвета, Размер L, Новинка"
    },
    new()
    {
        Id = 4, Name = "Платье", Category = CategoryRepository.Categories[4], CategoryId = CategoryRepository.Categories[4].Id,
        Description = "Платье бежового цвета, свадьбное, Размер Женский, Новинка"
    },
    new()
    {
        Id = 5, Name = "Джинсы", Category = CategoryRepository.Categories[4], CategoryId = CategoryRepository.Categories[4].Id,
        Description = "Джинсы обычного цвета, Размер L, В тренде"
    }
};


var builder = WebApplication.CreateBuilder(args);
builder.Services.AddSwaggerGen();
builder.Services.AddControllers();

var app = builder.Build();

app.UseRouting();

app.UseSwagger();
app.UseSwaggerUI();

app.MapControllers();

app.MapGet("/api/categories", ()=> categories);

app.MapGet("/api/categories/{id}", (int id) =>
{
    Category? categ = categories.FirstOrDefault(u => u.Id == id);
    if (categ == null)  return Results.NotFound(new { message = "Пользователь не найден" });
    return Results.Json(categ);
});

app.MapDelete("/api/categories/{id}", (int id) =>
{
    Category? category = categories.FirstOrDefault(u => u.Id == id);
 
    if (category == null) return Results.NotFound(new { message = "Пользователь не найден" });
 
    categories.Remove(category);
    return Results.Json(category);
});

app.MapPost("/api/categories", (Category categ)=>{
 
    categ.Id = 7;
    categories.Add(categ);
    return categ;
});

app.MapGet("/api/products", ()=> products);

app.MapGet("/api/products/{id}", (int id) =>
{
    var pro = products.FirstOrDefault(u => u.Id == id);
    return pro == null ? Results.NotFound(new { message = "Пользователь не найден" }) : Results.Json(pro);
});

app.MapDelete("/api/products/{id}", (int id) =>
{
    var product = products.FirstOrDefault(u => u.Id == id);
 
    if (product == null) return Results.NotFound(new { message = "Пользователь не найден" });
    products.Remove(product);
    return Results.Json(products);
});

app.MapPost("/api/products", (Product product)=>{
 
    product.Id = 6;
    products.Add(product);
    return product;
});

app.Run();
