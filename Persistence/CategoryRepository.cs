using Zadanie_1._1.Models;

namespace Zadanie_1._1.Persistence;

public static class CategoryRepository
{
    public static readonly List<Category> Categories = new()
    {
        new() { Id = 1, Name = "Размер", AdditionalPar = "XL"},
        new() { Id = 2, Name = "Цвет", AdditionalPar = "Синий"},
        new() { Id = 3, Name = "Цена", AdditionalPar = "50"},
        new() { Id = 4, Name = "Новинка", AdditionalPar = "New"},
        new() { Id = 5, Name = "В тренде", AdditionalPar = "CalvinKlein"},
    };

    public static int GetNewId() => 
        new Random().Next(0, 50000);
}