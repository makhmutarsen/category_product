using Microsoft.AspNetCore.Mvc;
using Zadanie_1._1.Models;
using Zadanie_1._1.Persistence;


public static class ProductRepository
{
    public static readonly List<Product> Products = new()
    {
        new()
        {
            Id = 1, Name = "Рубашка", Category = CategoryRepository.Categories[0], CategoryId = CategoryRepository.Categories[0].Id,
            Description = "Рубашка белого цвета, Размер XL"
        },
        new()
        {
            Id = 2, Name = "Футболка", Category = CategoryRepository.Categories[1], CategoryId = CategoryRepository.Categories[1].Id,
            Description = "Футболка черного цвета, Размер L, Новинка"
        },
        new()
        {
            Id = 3, Name = "Брюки", Category = CategoryRepository.Categories[2], CategoryId = CategoryRepository.Categories[2].Id,
            Description = "Брюки бежового цвета, Размер L, Новинка"
        },
        new()
        {
            Id = 4, Name = "Платье", Category = CategoryRepository.Categories[4], CategoryId = CategoryRepository.Categories[4].Id,
            Description = "Платье бежового цвета, свадьбное, Размер Женский, Новинка"
        },
        new()
        {
            Id = 5, Name = "Джинсы", Category = CategoryRepository.Categories[4], CategoryId = CategoryRepository.Categories[4].Id,
            Description = "Джинсы обычного цвета, Размер L, В тренде"
        }
    };
    public static int GetNewId() => 
        new Random().Next(0, 50000);
}